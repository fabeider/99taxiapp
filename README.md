# README #

Este projeto foi criado como um exemplo minimalista de uma applicativo de chamada de táxis.

Para criá-lo, foram usadas bibliotecas nativas em Swift, com exceção da funcionalidade de agrupamento.
Para o agrupamento foi utilizada a biblioteca FBAnnotationClusteringSwift, de código aberto e livre, disponível em [FBAnnotationClusteringSwift](https://github.com/ribl/FBAnnotationClusteringSwift)