//
//  ViewControllerAnnotationExt.swift
//  TaxiApp
//
//  Created by Fabio Beider on 19/11/15.
//  Copyright © 2015 Fabio Beider. All rights reserved.
//

import Foundation
import MapKit

extension ViewController: MKMapViewDelegate {
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        
        if annotation.isKindOfClass(FBAnnotationCluster) {
            reuseId = "Cluster"
            var clusterView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
            clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId)
            
            return clusterView
            
        } else if let annotation = annotation as? TaxiAnnotation {
            reuseId = "taxi"
            var view: TaxiAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
                as? TaxiAnnotationView {
                    dequeuedView.annotation = annotation
                    view = dequeuedView
            } else {
                view = TaxiAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .DetailDisclosure) as UIView
            }
            return view
        }
        
        return nil
    }
}