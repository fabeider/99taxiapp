//
//  URLSessionHelper.swift
//  TaxiApp
//
//  Created by Fabio Beider on 20/11/15.
//  Copyright © 2015 Fabio Beider. All rights reserved.
//

import Foundation

class LearnNSURLSession: NSObject, NSURLSessionDelegate, NSURLSessionTaskDelegate {
    
    typealias CallbackBlock = (result: String, error: String?) -> ()
    
    var callback: CallbackBlock = {
        (resultString, error) -> Void in
        if error == nil {
            print(resultString)
        } else {
            print(error)
        }
    }
    
    func httpGet(request: NSMutableURLRequest!, callback: (String,
        String?) -> Void) {
            
            let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
            let session = NSURLSession(configuration: configuration, delegate: self,
                delegateQueue:NSOperationQueue.mainQueue())
            let task = session.dataTaskWithRequest(request){
                (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
                if error != nil {
                    callback("", error!.localizedDescription)
                } else {
                    let result = NSString(data: data!, encoding:
                        NSASCIIStringEncoding)!
                    callback(result as String, nil)
                }
            }
            task.resume()
    }
}