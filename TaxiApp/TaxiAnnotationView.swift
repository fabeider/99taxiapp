//
//  TaxiAnnotationView.swift
//  TaxiApp
//
//  Created by Fabio Beider on 19/11/15.
//  Copyright © 2015 Fabio Beider. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class TaxiAnnotationView: MKAnnotationView{
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        let taxiAnnotation = self.annotation as! TaxiAnnotation
        
        if(taxiAnnotation.driverAvailable){
            image = UIImage (named: "FreeTaxiIcon")
        }
        else{
            // image = UIImage (named: "OccupiedTaxiIcon")
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
}