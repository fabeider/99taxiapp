//
//  ViewControllerClusterExt.swift
//  TaxiApp
//
//  Created by Fabio Beider on 26/11/15.
//  Copyright © 2015 Fabio Beider. All rights reserved.
//

import Foundation
import UIKit

extension ViewController : FBClusteringManagerDelegate {
    
    func cellSizeFactorForCoordinator(coordinator:FBClusteringManager) -> CGFloat{
        return 1.0
    }
}