
//
//  ViewController.swift
//  TaxiApp
//
//  Created by Fabio Beider on 19/11/15.
//  Copyright © 2015 Fabio Beider. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, NSURLConnectionDataDelegate, CLLocationManagerDelegate {

    typealias Edges = (ne: CLLocationCoordinate2D, sw: CLLocationCoordinate2D)
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var refresh: UIToolbar!
    
    let regionRadius: CLLocationDistance = 1000
    
    var data = NSMutableData()
    var locationManager = CLLocationManager()
    var userLocation = CLLocation()
    var clusterManager = FBClusteringManager()
    
    @IBAction func refresh(sender: AnyObject) {
        updateTaxis()
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        mapView.setRegion(region, animated: true)
    }
    
    func startUpdateUserLocation(){
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        
        userLocation = location
    }
    
    func edgePoints() -> Edges {
        let nePoint = CGPoint(x: mapView.bounds.maxX, y: mapView.bounds.origin.y)
        let swPoint = CGPoint(x: mapView.bounds.minX, y: mapView.bounds.maxY)
        let neCoord = mapView.convertPoint(nePoint, toCoordinateFromView: mapView)
        let swCoord = mapView.convertPoint(swPoint, toCoordinateFromView: mapView)
        return (ne: neCoord, sw: swCoord)
    }
    
    func getTaxis(){
        
        self.data = NSMutableData(data: NSMutableData())
        let edges = edgePoints()
        let urlPath: String = String(format: "https://api.99taxis.com/lastLocations?sw=%f,%f&ne=%f,%f", edges.sw.latitude, edges.sw.longitude, edges.ne.latitude, edges.ne.longitude)
        let url: NSURL = NSURL(string: urlPath)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        let connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)!
        connection.start()
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData){
        self.data.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        
        var annotationExists: Bool
        let jsonResult = JsonHelper.parseJSON(data)
        
        var annotationArray:[TaxiAnnotation] = clusterManager.allAnnotations() as! [TaxiAnnotation]
        
        for driver in jsonResult {
            
            let latitude = (driver["latitude"] as! NSNumber).doubleValue
            let longitude = (driver["longitude"] as! NSNumber).doubleValue
            let coordinate = CLLocationCoordinate2DMake(CLLocationDegrees(latitude), CLLocationDegrees(longitude))
            
            let driverId = (driver["driverId"] as! NSNumber).integerValue
            let driverAvailable = (driver["driverAvailable"] as! NSNumber).boolValue
            
            annotationExists = false
            if (annotationArray.count > 0){
                for existingAnnotation in annotationArray{
                    if (existingAnnotation.getDriverId() == driverId){
                        annotationExists = true
                        existingAnnotation.updateCoordinates(coordinate)
                        break
                    }
                }
            }
            
            if (!annotationExists){
                let annotation = TaxiAnnotation(driverId :driverId, driverAvailable: driverAvailable, coordinate: coordinate)
                annotationArray.append(annotation)
            }
        }
        
        clusterManager.setAnnotations(annotationArray)
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        clusterManager.delegate = self
        
        checkLocationAuthorizationStatus()
        startUpdateUserLocation()
    }
    
    // must be internal or public.
    func updateTaxis() {
        getTaxis()
        
        NSOperationQueue().addOperationWithBlock({
            
            let mapBoundsWidth = Double(self.mapView.bounds.size.width)
            let mapRectWidth:Double = self.mapView.visibleMapRect.size.width
            let scale:Double = mapBoundsWidth / mapRectWidth
            let annotationArray = self.clusterManager.clusteredAnnotationsWithinMapRect(self.mapView.visibleMapRect, withZoomScale:scale)
            self.clusterManager.displayAnnotations(annotationArray, onMapView:self.mapView)
            
        })

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
        centerMapOnLocation(self.userLocation)
        updateTaxis()
        _ = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: "updateTaxis", userInfo: nil, repeats: true)
    }
    
    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        updateTaxis()
        
    }
}

