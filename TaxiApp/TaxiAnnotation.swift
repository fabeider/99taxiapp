//
//  TaxiAnnotation.swift
//  TaxiApp
//
//  Created by Fabio Beider on 19/11/15.
//  Copyright © 2015 Fabio Beider. All rights reserved.
//

import Foundation
import MapKit

class TaxiAnnotation: NSObject, MKAnnotation {
    let driverId: Int
    let driverAvailable: Bool
    dynamic var coordinate: CLLocationCoordinate2D
    
    var title: String?
    
    init(driverId: Int, driverAvailable: Bool, coordinate: CLLocationCoordinate2D) {
        self.driverId = driverId
        self.driverAvailable = driverAvailable
        self.coordinate = coordinate
        self.title = String(format: "Taxista: %d", driverId)

        super.init()
    }
    
    func updateCoordinates(newCoordinate: CLLocationCoordinate2D){
        UIView.animateWithDuration(0.25) {
            self.coordinate = newCoordinate
        }
    }
    
    func getDriverId() -> Int
    {
        return driverId
    }
}