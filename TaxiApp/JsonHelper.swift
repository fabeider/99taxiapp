//
//  JSON.swift
//  TaxiApp
//
//  Created by Fabio Beider on 19/11/15.
//  Copyright © 2015 Fabio Beider. All rights reserved.
//

import Foundation
class JsonHelper: NSObject{
    
    class func parseJSON(inputData: NSData) -> Array<NSDictionary>{
        
        do{
            let boardsDictionary = try NSJSONSerialization.JSONObjectWithData(inputData, options: NSJSONReadingOptions.MutableContainers) as! Array<NSDictionary>
            
            return boardsDictionary
        } catch {
            return Array()
        }
        
    }
}